﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ModeloApi.Models
{
    [Table("NOM_POSICION")]
    public partial class NomPosicion
    {
        [Key]
        public int IdPosicion { get; set; }
        [StringLength(128)]
        public string NombrePosicion { get; set; }
        public bool BorradoPosicion { get; set; }
    }
}
