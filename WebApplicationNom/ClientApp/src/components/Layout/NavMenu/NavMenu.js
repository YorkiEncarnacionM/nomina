import React, { Component } from 'react';
import {  Navbar, NavbarBrand, NavLink } from 'reactstrap';
import { Link } from 'react-router-dom';
import Cookies from 'universal-cookie';
import './NavMenu.css';
import clsx from 'clsx';
import { Route } from 'react-router';
import { makeStyles } from '@material-ui/core/styles';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import MailIcon from '@material-ui/icons/Mail';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import BuildIcon from '@material-ui/icons/Build';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import Grid from '@material-ui/core/Grid';
import { Home } from '../../Home/Home';
import { Counter } from '../../Counter/Counter';


const cookies = new Cookies();


const useStyles = makeStyles({
    list: {
        width: 250,
    },
    fullList: {
        width: 'auto',
    },

});


export default function SwipeableTemporaryDrawer() {
    const classes = useStyles();

    const [state, setState] = React.useState({
        top: false,
        left: false,
        bottom: false,
        right: false,
    });

    const toggleDrawer = (anchor, open) => (event) => {
        if (event && event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
            return;
        }

        setState({ ...state, [anchor]: open });
    };

    const list = (anchor) => (
        <div
            className={clsx(classes.list, {
                [classes.fullList]: anchor === 'top' || anchor === 'bottom',
            })}
            role="presentation"
            onClick={toggleDrawer(anchor, false)}
            onKeyDown={toggleDrawer(anchor, false)}
        >
            <List>
                {['Inbox', 'Starred', 'Send email', 'Drafts'].map((text, index) => (
                    <ListItem button key={text}>
                        <ListItemIcon>{index % 2 === 0 ? <InboxIcon /> : <MailIcon />}</ListItemIcon>
                        <ListItemText primary={text} />
                    </ListItem>
                ))}
            </List>
            <Divider />
            <List>
                {['All mail', 'Trash', 'Spam'].map((text, index) => (
                    <ListItem button key={text}>
                        <ListItemIcon>{index % 2 === 0 ? <InboxIcon /> : <MailIcon />}</ListItemIcon>
                        <ListItemText primary={text} />
                    </ListItem>
                ))}
            </List>
        </div>
    );

    return (
        <div>
            {['left'].map((anchor) => (
                <React.Fragment key={anchor}>
                    <IconButton aria-label="add an alarm" onClick={toggleDrawer(anchor, true)}>
                        <MenuIcon/>
                    </IconButton>
                    <SwipeableDrawer
                        anchor={anchor}
                        open={state[anchor]}
                        onClose={toggleDrawer(anchor, false)}
                        onOpen={toggleDrawer(anchor, true)}
                    >
                        {list(anchor)}
                    </SwipeableDrawer>
                </React.Fragment>
            ))}
        </div>
    );

}


export class NavMenu extends Component {
  static displayName = NavMenu.name;


  constructor (props) {
    super(props);

    this.toggleNavbar = this.toggleNavbar.bind(this);
    this.state = {
      collapsed: true
    };
  }

  toggleNavbar () {
    this.setState({
      collapsed: !this.state.collapsed
    });
  }

  cerrarSesion=()=>{
      cookies.remove('id',)
      cookies.remove('id', { path: "/" }); 
      cookies.remove('nombre', { path: "/" }); 
      cookies.remove('apellido', { path: "/" }); 
      cookies.remove('email', { path: "/" }); 
      cookies.remove('usuario', { path: "/" }); 
      cookies.remove('token', { path: "/" });  

      window.location.href = "./";
    }

  componentDidMount() {
      if (!cookies.get('id')) {
          window.location.href = "./";
      } 

      
    }

    render() {
    const hello = 'Bienvenido ' + cookies.get('nombre') + ' ' + cookies.get('apellido');

    return (
      <header>
        <Navbar className="navbar-expand-sm navbar-toggleable-sm ng-white border-bottom box-shadow mb-3" light>
                <Grid
                    container
                    direction="row"
                    justify="flex-start"
                    alignItems="center"
                >
                    <SwipeableTemporaryDrawer />
                    <NavbarBrand tag={Link} to="/app">WebApplicationNom</NavbarBrand>
                    <NavLink tag={Link} className="text-dark" to="/home">Home</NavLink>
                    <NavLink tag={Link} className="text-dark" to="/counter">Counter</NavLink>
                    <NavLink tag={Link} className="text-dark" to="/posicion">posicion</NavLink>
                </Grid>
                <Grid
                    container
                    direction="row"
                    justify="flex-end"
                    alignItems="center"
                >
                    <div>{hello}</div>
                    <IconButton>
                        <BuildIcon />
                    </IconButton>
                    <IconButton onClick={() => this.cerrarSesion()}>
                        <ExitToAppIcon />
                    </IconButton>  
                </Grid>

        </Navbar>
      </header>
    );
  }
}
