﻿import React, { Component } from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';


export default class Circularprog extends React.Component {
    render() {
        const { show } = this.props;

        return (
            <CircularProgress variant="indeterminate" disableShrink={show} />
         );
    }
}

