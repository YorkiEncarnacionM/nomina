﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using ModeloApi.Context;
using ModeloApi.Helpers;
using ModeloApi.Models;
using ModeloApi.Models.DTOS;

namespace ModeloApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private readonly IConfiguration _config;
        private readonly ContextBD _context;

        private List<SysUsuario> appUsers = new List<SysUsuario>
        {
                //new User { FullName = “Vaibhav Bhapkar”, UserName = “admin”, Password = “1234”, UserRole = “Admin” },
                //new User { FullName = “Test User”, UserName = “user”, Password = “1234”, UserRole = “User” }
                new SysUsuario{NombreUsuario="Enmanuel",ApellidoUsuario="Encarnacion",EmailUsuario="yorki.encarnacion13@gmail.com",Usuario="Admin",ClaveUsuario="12345",TokenUsuario="1",BorradoUsuario=false}
        };

        public LoginController(IConfiguration config, ContextBD context)
        {
            _config = config;
            _context = context;
        }

        [HttpPost]
        [AllowAnonymous]
        public IActionResult Login([FromBody] Login login)
        {
            IActionResult response = Unauthorized();

            SysUsuario user = AuthenticateUser(login);
            if (user != null)
            {
                var tokenString = GenerateJWTToken(user);

                UpdateTokenUser(tokenString, user);

                response = Ok(new
                {
                    //token = tokenString,
                    userDetails = user,
                });
            }
            return response;
        }

        SysUsuario AuthenticateUser(Login loginCredentials)
        {
            SysUsuario User = _context.SysUsuario.Where(x => x.Usuario == loginCredentials.Usuario).FirstOrDefault();

            var isValid = Passwords.CheckRawPassword(loginCredentials.ClaveUsuario, User.ClaveUsuario, User.SecurityKey, Constants.Peppers);

            if (isValid == true)
            {
                return User;
            }
            else
            {
                return null;
            }

        }

        string GenerateJWTToken(SysUsuario userInfo)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:SecretKey"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            var claims = new[]
            {
            new Claim(JwtRegisteredClaimNames.Sub, userInfo.Usuario),
            new Claim("fullName", userInfo.NombreUsuario.ToString()+' '+userInfo.ApellidoUsuario.ToString()),
            new Claim("role",userInfo.EmailUsuario),
            new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
            };

            var token = new JwtSecurityToken(
            issuer: _config["Jwt:Issuer"],
            audience: _config["Jwt:Audience"],
            claims: claims,
            expires: DateTime.Now.AddMinutes(30),
            signingCredentials: credentials
            );
            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        bool UpdateTokenUser(string Token, SysUsuario usuario)
        {
            usuario.TokenUsuario = Token;
            _context.Entry(usuario).State = EntityState.Modified;
            //_context.Usuario.Update(usuario);
            _context.SaveChangesAsync();

            return true;
        }

        [HttpPost("CrearUsuario")]
        [AllowAnonymous]
        public async Task<ActionResult<SysUsuario>> CrearUsuario(SysUsuario Usuario)
        {
            var PasswordComposite = Passwords.GenerateCredentials(Constants.Peppers, Usuario.ClaveUsuario);
            //var PasswordComposite = Passwords.GenerateCredentials(Constants.Peppers, "admin");

            Usuario.ClaveUsuario = PasswordComposite.Password;
            Usuario.SecurityKey = PasswordComposite.Salt;

            _context.SysUsuario.Add(Usuario);
            await _context.SaveChangesAsync();

            return Usuario;
        }
    }
}
