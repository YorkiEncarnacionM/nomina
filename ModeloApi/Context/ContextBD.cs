﻿using System;
using System.Configuration;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.IdentityModel.Protocols;
using ModeloApi.Models;

namespace ModeloApi.Context
{
    public partial class ContextBD : DbContext
    {
        public ContextBD()
        {
        }

        public ContextBD(DbContextOptions<ContextBD> options)
            : base(options)
        {
        }

        public virtual DbSet<AgnAgente> AgnAgente { get; set; }
        public virtual DbSet<AgnDireccion> AgnDireccion { get; set; }
        public virtual DbSet<AgnEmpleados> AgnEmpleados { get; set; }
        public virtual DbSet<AgnMedioContacto> AgnMedioContacto { get; set; }
        public virtual DbSet<AgnTipoMedioContato> AgnTipoMedioContato { get; set; }
        public virtual DbSet<GeoCategoriaSubdivision> GeoCategoriaSubdivision { get; set; }
        public virtual DbSet<GeoConversionMonedas> GeoConversionMonedas { get; set; }
        public virtual DbSet<GeoIsoMonedas> GeoIsoMonedas { get; set; }
        public virtual DbSet<GeoMonedaPais> GeoMonedaPais { get; set; }
        public virtual DbSet<GeoPais> GeoPais { get; set; }
        public virtual DbSet<GeoPaisSubdivision> GeoPaisSubdivision { get; set; }
        public virtual DbSet<GeoTelFormat> GeoTelFormat { get; set; }
        public virtual DbSet<NomPosicion> NomPosicion { get; set; }
        public virtual DbSet<SysUsuario> SysUsuario { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {

                optionsBuilder.UseSqlServer(ConfigurationManager.ConnectionStrings["Database"].ConnectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AgnAgente>(entity =>
            {
                entity.HasKey(e => e.IdAgn)
                    .HasName("PK__AGN_AGEN__0E2BC6C46957E712");
            });

            modelBuilder.Entity<AgnDireccion>(entity =>
            {
                entity.HasKey(e => e.IdDireccionAgn)
                    .HasName("PK__AGN_DIRE__D80FAE93CF0503FB");

                entity.Property(e => e.CodigoPaisAlfa2)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CodigoSubdivision)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.HasOne(d => d.CodigoPaisAlfa2Navigation)
                    .WithMany(p => p.AgnDireccion)
                    .HasForeignKey(d => d.CodigoPaisAlfa2)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__AGN_DIREC__Codig__787EE5A0");

                entity.HasOne(d => d.CodigoSubdivisionNavigation)
                    .WithMany(p => p.AgnDireccion)
                    .HasForeignKey(d => d.CodigoSubdivision)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__AGN_DIREC__Codig__778AC167");

                entity.HasOne(d => d.IdAgnNavigation)
                    .WithMany(p => p.AgnDireccion)
                    .HasForeignKey(d => d.IdAgn)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__AGN_DIREC__IdAgn__797309D9");
            });

            modelBuilder.Entity<AgnEmpleados>(entity =>
            {
                entity.HasKey(e => e.IdAgn)
                    .HasName("PK__AGN_EMPL__CE6D8B9E96583ADA");

                entity.Property(e => e.IdAgn).ValueGeneratedNever();

                entity.Property(e => e.NoSeguridadSocialEmpleado)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.TasaIsrMensual).HasDefaultValueSql("((0))");

                entity.HasOne(d => d.IdAgnNavigation)
                    .WithOne(p => p.AgnEmpleados)
                    .HasForeignKey<AgnEmpleados>(d => d.IdAgn)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_AGN_EMPLEADOS_NOM_POSICION");
            });

            modelBuilder.Entity<AgnMedioContacto>(entity =>
            {
                entity.HasKey(e => e.IdMedioContacto)
                    .HasName("PK__AGN_MEDI__3E86CE3C09243477");

                entity.Property(e => e.MedioContacto).IsUnicode(false);

                entity.HasOne(d => d.IdAgnNavigation)
                    .WithMany(p => p.AgnMedioContacto)
                    .HasForeignKey(d => d.IdAgn)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__AGN_MEDIO__IdAgn__7B5B524B");

                entity.HasOne(d => d.IdTipoMedioContactoNavigation)
                    .WithMany(p => p.AgnMedioContacto)
                    .HasForeignKey(d => d.IdTipoMedioContacto)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__AGN_MEDIO__IdTip__7C4F7684");
            });

            modelBuilder.Entity<AgnTipoMedioContato>(entity =>
            {
                entity.HasKey(e => e.IdTipoMedioContacto)
                    .HasName("PK__AGN_TIPO__6DB05631CBA704AA");
            });

            modelBuilder.Entity<GeoCategoriaSubdivision>(entity =>
            {
                entity.HasKey(e => e.IdCategoriaSubdivision)
                    .HasName("PK__GEO_CATE__C6213D5295D49810");

                entity.Property(e => e.ProcesarCategoria).IsUnicode(false);
            });

            modelBuilder.Entity<GeoConversionMonedas>(entity =>
            {
                entity.HasKey(e => e.IdConversionMoneda)
                    .HasName("PK__GEO_CONV__D2A7E154DA15A810");

                entity.HasIndex(e => new { e.MonedaOrigen, e.MonedaDestino, e.FechaTasaCambio })
                    .HasName("UQ__GEO_CONV__D092F3E454B83CFE")
                    .IsUnique();

                entity.Property(e => e.FechaInConversionMoneda).HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.MonedaDestinoNavigation)
                    .WithMany(p => p.GeoConversionMonedasMonedaDestinoNavigation)
                    .HasForeignKey(d => d.MonedaDestino)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__GEO_CONVE__Moned__4D94879B");

                entity.HasOne(d => d.MonedaOrigenNavigation)
                    .WithMany(p => p.GeoConversionMonedasMonedaOrigenNavigation)
                    .HasForeignKey(d => d.MonedaOrigen)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__GEO_CONVE__Moned__4CA06362");
            });

            modelBuilder.Entity<GeoIsoMonedas>(entity =>
            {
                entity.HasKey(e => e.IdMoneda)
                    .HasName("PK__GEO_ISO___AA690671078BC16E");

                entity.HasIndex(e => e.CodigoMoneda)
                    .HasName("UQ__GEO_ISO___8E6DB164DCDAB8A4")
                    .IsUnique();

                entity.Property(e => e.CodigoMoneda)
                    .IsUnicode(false)
                    .IsFixedLength();
            });

            modelBuilder.Entity<GeoMonedaPais>(entity =>
            {
                entity.HasKey(e => e.IdMonedaPais)
                    .HasName("PK__GEO_MONE__6678E7FED9E11A29");

                entity.Property(e => e.CodigoPaisAlfa2)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.HasOne(d => d.CodigoPaisAlfa2Navigation)
                    .WithMany(p => p.GeoMonedaPais)
                    .HasForeignKey(d => d.CodigoPaisAlfa2)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__GEO_MONED__Codig__4E88ABD4");

                entity.HasOne(d => d.IdMonedaNavigation)
                    .WithMany(p => p.GeoMonedaPais)
                    .HasForeignKey(d => d.IdMoneda)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__GEO_MONED__IdMon__4F7CD00D");
            });

            modelBuilder.Entity<GeoPais>(entity =>
            {
                entity.HasKey(e => e.CodigoPaisAlfa2)
                    .HasName("PK__GEO_PAIS__1B364E49EAE09B29");

                entity.HasIndex(e => e.CodigoPaisAlfa3)
                    .HasName("UQ__GEO_PAIS__1B364DB777025486")
                    .IsUnique();

                entity.HasIndex(e => e.CodigoPaisNum)
                    .HasName("UQ__GEO_PAIS__78BCBD00DA696820")
                    .IsUnique();

                entity.Property(e => e.CodigoPaisAlfa2)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.ActivoPais).HasDefaultValueSql("((1))");

                entity.Property(e => e.CodigoPaisAlfa3)
                    .IsUnicode(false)
                    .IsFixedLength();
            });

            modelBuilder.Entity<GeoPaisSubdivision>(entity =>
            {
                entity.HasKey(e => e.CodigoSubdivision)
                    .HasName("PK__GEO_PAIS__79BF245A0BC7BDC3");

                entity.Property(e => e.CodigoSubdivision)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CodigoPaisAlfa2)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.SubdivisionMadre)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.HasOne(d => d.CodigoPaisAlfa2Navigation)
                    .WithMany(p => p.GeoPaisSubdivision)
                    .HasForeignKey(d => d.CodigoPaisAlfa2)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__GEO_PAIS___Codig__5070F446");

                entity.HasOne(d => d.IdCategoriaSubdivisionNavigation)
                    .WithMany(p => p.GeoPaisSubdivision)
                    .HasForeignKey(d => d.IdCategoriaSubdivision)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__GEO_PAIS___IdCat__5165187F");

                entity.HasOne(d => d.SubdivisionMadreNavigation)
                    .WithMany(p => p.InverseSubdivisionMadreNavigation)
                    .HasForeignKey(d => d.SubdivisionMadre)
                    .HasConstraintName("FK__GEO_PAIS___Subdi__52593CB8");
            });

            modelBuilder.Entity<GeoTelFormat>(entity =>
            {
                entity.HasKey(e => e.IdFormatTel)
                    .HasName("PK__GEO_TEL___F4DE7B9246261460");

                entity.HasIndex(e => e.CodigoPaisAlfa2)
                    .HasName("UQ__GEO_TEL___1B364E4852C0285B")
                    .IsUnique();

                entity.Property(e => e.CodigoPaisAlfa2)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.FormatoFijo).IsUnicode(false);

                entity.Property(e => e.FormatoMovil).IsUnicode(false);

                entity.Property(e => e.MascaraFijo).IsUnicode(false);

                entity.Property(e => e.MascaraMovil).IsUnicode(false);

                entity.HasOne(d => d.CodigoPaisAlfa2Navigation)
                    .WithOne(p => p.GeoTelFormat)
                    .HasForeignKey<GeoTelFormat>(d => d.CodigoPaisAlfa2)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__GEO_TEL_F__Codig__534D60F1");
            });

            modelBuilder.Entity<SysUsuario>(entity =>
            {
                entity.HasKey(e => e.IdUsuario)
                    .HasName("PK_Usuario");

                entity.Property(e => e.ApellidoUsuario).IsUnicode(false);

                entity.Property(e => e.ClaveUsuario).IsUnicode(false);

                entity.Property(e => e.EmailUsuario).IsUnicode(false);

                entity.Property(e => e.FechaCreacionUsuario).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.NombreUsuario).IsUnicode(false);

                entity.Property(e => e.TokenUsuario).IsUnicode(false);

                entity.Property(e => e.Usuario).IsUnicode(false);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
