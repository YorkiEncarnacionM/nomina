﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ModeloApi.Context;
using ModeloApi.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ModeloApi.Controllers.NOM
{
    [Route("api/[controller]")]
    [ApiController]
    public class PosicionController : ControllerBase
    {
        private readonly ContextBD _context;

        public PosicionController(ContextBD context)
        {
            _context = context;
        }


        // GET: api/<PosicionController>
        [HttpGet]
        [Authorize]
        public async Task<ActionResult<IEnumerable<NomPosicion>>> GetPosicion()
        {
            return await _context.NomPosicion.ToListAsync();
        }

        // GET api/<PosicionController>/5
        [HttpGet("{id}")]
        [Authorize]
        public async Task<ActionResult<NomPosicion>> GetPosicion(int id)
        {
            var posicion = await _context.NomPosicion.FindAsync(id);

            if (posicion == null)
            {
                return NotFound();
            }

            return posicion;
        }

        // POST api/<PosicionController>
        [HttpPost]
        [Authorize]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<PosicionController>/5
        [HttpPut("{id}")]
        [Authorize]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<PosicionController>/5
        [HttpDelete("{id}")]
        [Authorize]
        public void Delete(int id)
        {
        }
    }
}
