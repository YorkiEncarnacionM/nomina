﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ModeloApi.Models
{
    [Table("AGN_EMPLEADOS")]
    public partial class AgnEmpleados
    {
        [Key]
        public int IdAgn { get; set; }
        public int IdPosicion { get; set; }
        [StringLength(9)]
        public string NoSeguridadSocialEmpleado { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime FechaIngresoEmpleado { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? FechaBajaEmpleado { get; set; }
        [Column(TypeName = "money")]
        public decimal SueldoMensual { get; set; }
        [Column(TypeName = "money")]
        public decimal PrecioHora { get; set; }
        [Column(TypeName = "money")]
        public decimal PrecioHoraExtra { get; set; }
        [Column(TypeName = "numeric(4, 2)")]
        public decimal? TasaIsrMensual { get; set; }

        [ForeignKey(nameof(IdAgn))]
        [InverseProperty(nameof(AgnAgente.AgnEmpleados))]
        public virtual AgnAgente IdAgnNavigation { get; set; }
    }
}
