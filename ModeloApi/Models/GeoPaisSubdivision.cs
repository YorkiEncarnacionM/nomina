﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ModeloApi.Models
{
    [Table("GEO_PAIS_SUBDIVISION")]
    public partial class GeoPaisSubdivision
    {
        public GeoPaisSubdivision()
        {
            AgnDireccion = new HashSet<AgnDireccion>();
            InverseSubdivisionMadreNavigation = new HashSet<GeoPaisSubdivision>();
        }

        [Key]
        [StringLength(5)]
        public string CodigoSubdivision { get; set; }
        [Required]
        [StringLength(2)]
        public string CodigoPaisAlfa2 { get; set; }
        public int IdCategoriaSubdivision { get; set; }
        [StringLength(5)]
        public string SubdivisionMadre { get; set; }
        [Required]
        [StringLength(128)]
        public string NombreSubdivision { get; set; }
        [Column(TypeName = "decimal(10, 6)")]
        public decimal? LatitudPaisSubdivision { get; set; }
        [Column(TypeName = "decimal(10, 6)")]
        public decimal? LongitudPaisSubdivision { get; set; }

        [ForeignKey(nameof(CodigoPaisAlfa2))]
        [InverseProperty(nameof(GeoPais.GeoPaisSubdivision))]
        public virtual GeoPais CodigoPaisAlfa2Navigation { get; set; }
        [ForeignKey(nameof(IdCategoriaSubdivision))]
        [InverseProperty(nameof(GeoCategoriaSubdivision.GeoPaisSubdivision))]
        public virtual GeoCategoriaSubdivision IdCategoriaSubdivisionNavigation { get; set; }
        [ForeignKey(nameof(SubdivisionMadre))]
        [InverseProperty(nameof(GeoPaisSubdivision.InverseSubdivisionMadreNavigation))]
        public virtual GeoPaisSubdivision SubdivisionMadreNavigation { get; set; }
        [InverseProperty("CodigoSubdivisionNavigation")]
        public virtual ICollection<AgnDireccion> AgnDireccion { get; set; }
        [InverseProperty(nameof(GeoPaisSubdivision.SubdivisionMadreNavigation))]
        public virtual ICollection<GeoPaisSubdivision> InverseSubdivisionMadreNavigation { get; set; }
    }
}
