﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ModeloApi.Context;
using ModeloApi.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ModeloApi.Controllers.AGN
{
    [Route("api/[controller]")]
    [ApiController]
    public class TiposContactoController : ControllerBase
    {
        private readonly ContextBD _context;

        public TiposContactoController(ContextBD context)
        {
            _context = context;
        }


        // GET: api/<TiposContactoController>
        [HttpGet]
        [Authorize]
        public async Task<ActionResult<IEnumerable<AgnTipoMedioContato>>> GetTipoMedioContato()
        {
            return await _context.AgnTipoMedioContato.ToListAsync();
        }

        // GET api/<TiposContactoController>/5
        [HttpGet("{id}")]
        [Authorize]
        public async Task<ActionResult<AgnTipoMedioContato>> GetTipoMedioContato(int id)
        {
            var tipoMedioContato = await _context.AgnTipoMedioContato.FindAsync(id);

            if (tipoMedioContato == null)
            {
                return NotFound();
            }

            return tipoMedioContato;
        }

        // POST api/<TiposContactoController>
        [HttpPost]
        [Authorize]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<TiposContactoController>/5
        [HttpPut("{id}")]
        [Authorize]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<TiposContactoController>/5
        [HttpDelete("{id}")]
        [Authorize]
        public void Delete(int id)
        {
        }
    }
}
