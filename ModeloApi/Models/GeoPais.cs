﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ModeloApi.Models
{
    [Table("GEO_PAIS")]
    public partial class GeoPais
    {
        public GeoPais()
        {
            AgnDireccion = new HashSet<AgnDireccion>();
            GeoMonedaPais = new HashSet<GeoMonedaPais>();
            GeoPaisSubdivision = new HashSet<GeoPaisSubdivision>();
        }

        [Key]
        [StringLength(2)]
        public string CodigoPaisAlfa2 { get; set; }
        [Required]
        [StringLength(3)]
        public string CodigoPaisAlfa3 { get; set; }
        public int CodigoPaisNum { get; set; }
        [Required]
        [StringLength(128)]
        public string NombrePais { get; set; }
        public byte[] BanderaPais { get; set; }
        public byte[] EscudoPais { get; set; }
        public int? PoblacionActualPais { get; set; }
        [Column(TypeName = "decimal(10, 6)")]
        public decimal? LatitudPais { get; set; }
        [Column(TypeName = "decimal(10, 6)")]
        public decimal? LongitudPais { get; set; }
        [Required]
        public bool? ActivoPais { get; set; }
        public bool LocalPais { get; set; }
        public bool BorradoPais { get; set; }

        [InverseProperty("CodigoPaisAlfa2Navigation")]
        public virtual GeoTelFormat GeoTelFormat { get; set; }
        [InverseProperty("CodigoPaisAlfa2Navigation")]
        public virtual ICollection<AgnDireccion> AgnDireccion { get; set; }
        [InverseProperty("CodigoPaisAlfa2Navigation")]
        public virtual ICollection<GeoMonedaPais> GeoMonedaPais { get; set; }
        [InverseProperty("CodigoPaisAlfa2Navigation")]
        public virtual ICollection<GeoPaisSubdivision> GeoPaisSubdivision { get; set; }
    }
}
