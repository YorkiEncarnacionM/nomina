﻿


import React, { Component } from "react";
import axios from 'axios';
import "./Login.css";
import Copyright from '../Layout/Copyright/Copyright';
import PopUpInformativo from '../../componentsGlb/popup/popupinformativo';
import Circularprog from '../../componentsGlb/progress/circularprogress';

import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Cookies from 'universal-cookie';

import Fade from '@material-ui/core/Fade';

const cookies = new Cookies();
const { createProxyMiddleware } = require('http-proxy-middleware');

function LocationLogin() {

    const headers = {
   

        
    };

    axios.get('https://geolocation-db.com/json/', {headers}
    ).then(res => {
        console.log(res.data);
    })

    //fetch('http://geolocation-db.com/json/', { mode: 'no-cors' })
    //    .then(function (response) {
    //        console.log(response);
    //    }).catch(function (error) {
    //        console.log('Request failed', error)
    //    });

    //const proxyurl = "https://cors-anywhere.herokuapp.com/";
    //const url = 'http://geolocation-db.com/json/'; // site that doesn’t send Access-Control-*
    //fetch(proxyurl + url).then((resp) => resp.json())
    //    .then(function (data) {
    //        console.log(data);
    //    })
    //    .catch(function (error) {
    //        console.log(error);
    //    }); 

}

const useStyles = makeStyles((theme) => ({
    root: {
        height: '100vh',
    },
    image: {
        backgroundImage: 'url(https://source.unsplash.com/random)',
        backgroundRepeat: 'no-repeat',
        backgroundColor:
            theme.palette.type === 'light' ? theme.palette.grey[50] : theme.palette.grey[900],
        backgroundSize: 'cover',
        backgroundPosition: 'center',
    },
    paper: {
        margin: theme.spacing(8, 4),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

class WindowsForm extends React.Component {
    state = {
        form: {
            Usuario: '',
            Password:''
        }, 
        enabled: true,
        login: false
    }


    handleChange = async e => {
        await this.setState({
            form: {
                ...this.state.form,
                [e.target.name]: e.target.value
            }
        });

        if (this.state.form.Usuario.length > 0 && this.state.form.Password.length > 0) {
            this.state.enabled = false
        } else {
            this.state.enabled = true
        }
           
        //console.log(this.state.form)
    }

    login=async()=> {


        const headers = {
            "Accept": "application/json",
            "Content-Type": "application/json",
            "data": JSON.stringify({
                Usuario: this.state.form.Usuario,
                ClaveUsuario: this.state.form.Password
            })
        };

        axios.post('/api/Login', JSON.stringify({
            Usuario: this.state.form.Usuario,
            ClaveUsuario: this.state.form.Password
        }), { headers }
        ).then(response => {
            var data = JSON.parse(response.request.response);

            //console.log(data["userDetails"]);

            cookies.set('id', data["userDetails"].idUsuario, {path: "/"});
            cookies.set('nombre', data["userDetails"].nombreUsuario, { path: "/" });
            cookies.set('apellido', data["userDetails"].apellidoUsuario, { path: "/" });
            cookies.set('email', data["userDetails"].emailUsuario, { path: "/" });
            cookies.set('usuario', data["userDetails"].usuario, { path: "/" });
            cookies.set('token', data["userDetails"].tokenUsuario, { path: "/" });

            window.location.href = "./app";

        }).catch(err => {
            if (err.response) {
                console.log(err.response);
                // client received an error response (5xx, 4xx)
            } else if (err.request) {

                // client never received a response, or request never left
            } else {
                // anything else
            }
        })






        //console.log(this.state.form.Usuario)
        //console.log(this.state.form.Password)

        //fetch('https://localhost:44336/api/Login', {
        //    method: 'POST',
        //    headers: {
        //        'Accept': 'application/json',
        //        'Content-Type': 'application/json'
        //    },
        //    body: JSON.stringify({
        //        Usuario: this.state.form.Usuario,
        //        ClaveUsuario: this.state.form.Password
        //    })

        //}).then((Response) => Response.json());

        //axios.get(`https://jsonplaceholder.typicode.com/users`)
        //    .then(res => {
        //        const persons = res.data;
        //        console.log(persons);

        //    })

       

        //axios({
        //    method: 'get',
        //    url: '/weatherforecast',
        //    headers: {
        //        "Accept": "application/json",
        //        "Content-Type": "application/json"
        //    }
        //}).then(function (response) {
        //    console.log(response)
        //});

        //axios.post(`https://jsonplaceholder.typicode.com/posts`, JSON.stringify({ Usuario: this.state.form.Usuario, ClaveUsuario: this.state.form.Password}), headers)
        //    .then(res => {
        //        const persons = res.data;
        //        console.log(persons);

        //    })

        //fetch('/api/Login', {
        //    method: 'post', body: {
        //        Usuario: this.state.form.Usuario,
        //        ClaveUsuario: this.state.form.Password
        //    }
        //}).then(function (response) {
        //        if (response.ok) {
        //            return response.text()
        //        } else {
        //            throw "Error en la llamada Ajax";
        //        }

        //    }).then(function (texto) {
        //        console.log(texto);
        //    })
        //    .catch(function (err) {
        //        console.log(err);
        //    });

        //axios({
        //    method: 'post',
        //    url: '/api/Login',
        //    data: {
        //        Usuario: this.state.form.Usuario,
        //        ClaveUsuario: this.state.form.Password
        //    },
        //    headers: {
        //        "Accept": "application/json",
        //        "Content-Type": "application/json"}
        //}).then(function (response) {
        //    console.log(response)
        //});


        
           

    }

    componentDidMount() {
        if (cookies.get('id')) {
            window.location.href = "./app";
        }
        LocationLogin();
    }

    render() {
        const { Usuario, Password } = this.state.form;

        //const enabled =
        //    Usuario.length > 0 &&
        //    Password.length > 0;


        return (

            <div>
                <TextField
                    variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    id="Usuario"
                    label="Usuario"
                    name="Usuario"
                    autoComplete="Usuario"
                    autoFocus
                    onChange={this.handleChange}
                />
                <TextField
                    variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    name="Password"
                    label="Contraseña"
                    type="Password"
                    id="Password"
                    autoComplete="current-password"
                    onChange={this.handleChange}
                />
                <Button
                    fullWidth
                    variant="contained"
                    color="primary"
                    disabled={this.state.enabled}
                    onClick={() => this.login()}
                >
                    Iniciar Sesión
                    <Circularprog show={false} />
                    
                </Button>
                <Grid container>
                    <Grid item xs>
                        <Link href="#" variant="body2">
                            ¿Se te olvidó tu contraseña?
                        </Link>
                    </Grid>

                </Grid>
                <PopUpInformativo Mesaje="AAAA"/>
            </div>         
        );

    }
}

export default function Login() {
    const classes = useStyles();


    return (
        
        <Grid container component="main" className={classes.root}>
            <CssBaseline />
            <Grid item xs={false} sm={4} md={7} className={classes.image} />
            <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
                <div className={classes.paper}>
                    <Avatar className={classes.avatar}>
                        <LockOutlinedIcon />
                    </Avatar>
                    <Typography component="h1" variant="h5">
                        Inicio
                    </Typography>
                    <form className={classes.form} noValidate>
                        <WindowsForm></WindowsForm>
                        
                        <Box mt={5}>
                            <Copyright />
                        </Box>
                    </form>
                </div>
            </Grid>
        </Grid>
    );
}


//export default function Login() {
//    const classes = useStyles();

///<CircularUnderLoad disabled={!enabled} />
//    return (

//        <Grid container component="main" className={classes.root}>
//            <CssBaseline />
//            <Grid item xs={false} sm={4} md={7} className={classes.image} />
//            <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
//                <div className={classes.paper}>
//                    <Avatar className={classes.avatar}>
//                        <LockOutlinedIcon />
//                    </Avatar>
//                    <Typography component="h1" variant="h5">
//                        Inicio
//                    </Typography>
//                    <form className={classes.form} noValidate>
//                        <TextField
//                            variant="outlined"
//                            margin="normal"
//                            required
//                            fullWidth
//                            id="usuario"
//                            label="Usuario"
//                            name="usuario"
//                            autoComplete="usuario"
//                            autoFocus
//                        />
//                        <TextField
//                            variant="outlined"
//                            margin="normal"
//                            required
//                            fullWidth
//                            name="password"
//                            label="Contraseña"
//                            type="password"
//                            id="password"
//                            autoComplete="current-password"
//                        />
//                        <Button
//                            type="submit"
//                            fullWidth
//                            variant="contained"
//                            color="primary"
//                            className={classes.submit}
//                        >
//                            Iniciar Sesión
//                        </Button>
//                        <Grid container>
//                            <Grid item xs>
//                                <Link href="#" variant="body2">
//                                    ¿Se te olvidó tu contraseña?
//                                </Link>
//                            </Grid>

//                        </Grid>
//                        <Box mt={5}>
//                            <Copyright />
//                        </Box>
//                    </form>
//                </div>
//            </Grid>
//        </Grid>
//    );
//}


//<Grid container component="main" className={classes.root}>
//    <CssBaseline />
//    <Grid item xs={false} sm={4} md={7} className={classes.image} />
//    <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
//        <div className={classes.paper}>
//            <Avatar className={classes.avatar}>
//                <LockOutlinedIcon />
//            </Avatar>
//            <Typography component="h1" variant="h5">
//                Inicio
//                    </Typography>
//            <form className={classes.form} noValidate>
//                <TextField
//                    variant="outlined"
//                    margin="normal"
//                    required
//                    fullWidth
//                    id="email"
//                    label="Usuario"
//                    name="email"
//                    autoComplete="email"
//                    autoFocus
//                />
//                <TextField
//                    variant="outlined"
//                    margin="normal"
//                    required
//                    fullWidth
//                    name="password"
//                    label="Contraseña"
//                    type="password"
//                    id="password"
//                    autoComplete="current-password"
//                />
//                <FormControlLabel
//                    control={<Checkbox value="remember" color="primary" />}
//                    label="Remember me"
//                />
//                <Button
//                    type="submit"
//                    fullWidth
//                    variant="contained"
//                    color="primary"
//                    className={classes.submit}
//                >
//                    Iniciar Sesión
//                        </Button>
//                <Grid container>
//                    <Grid item xs>
//                        <Link href="#" variant="body2">
//                            ¿Se te olvidó tu contraseña?
//                                </Link>
//                    </Grid>
//                    <Grid item>
//                        <Link href="#" variant="body2">
//                            {"Don't have an account? Sign Up"}
//                        </Link>
//                    </Grid>
//                </Grid>
//                <Box mt={5}>
//                    <Copyright />
//                </Box>
//            </form>
//        </div>
//    </Grid>
//</Grid>