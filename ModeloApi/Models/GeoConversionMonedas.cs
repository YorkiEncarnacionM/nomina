﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ModeloApi.Models
{
    [Table("GEO_CONVERSION_MONEDAS")]
    public partial class GeoConversionMonedas
    {
        [Key]
        public int IdConversionMoneda { get; set; }
        public int MonedaOrigen { get; set; }
        public int MonedaDestino { get; set; }
        [Column(TypeName = "date")]
        public DateTime FechaTasaCambio { get; set; }
        [Column(TypeName = "numeric(18, 10)")]
        public decimal MultiplicadorTasaCambio { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? FechaInConversionMoneda { get; set; }

        [ForeignKey(nameof(MonedaDestino))]
        [InverseProperty(nameof(GeoIsoMonedas.GeoConversionMonedasMonedaDestinoNavigation))]
        public virtual GeoIsoMonedas MonedaDestinoNavigation { get; set; }
        [ForeignKey(nameof(MonedaOrigen))]
        [InverseProperty(nameof(GeoIsoMonedas.GeoConversionMonedasMonedaOrigenNavigation))]
        public virtual GeoIsoMonedas MonedaOrigenNavigation { get; set; }
    }
}
