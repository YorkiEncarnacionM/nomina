﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ModeloApi.Models
{
    [Table("GEO_CATEGORIA_SUBDIVISION")]
    public partial class GeoCategoriaSubdivision
    {
        public GeoCategoriaSubdivision()
        {
            GeoPaisSubdivision = new HashSet<GeoPaisSubdivision>();
        }

        [Key]
        public int IdCategoriaSubdivision { get; set; }
        [Required]
        [StringLength(128)]
        public string NombreCategoriaSubdivision { get; set; }
        [StringLength(20)]
        public string ProcesarCategoria { get; set; }

        [InverseProperty("IdCategoriaSubdivisionNavigation")]
        public virtual ICollection<GeoPaisSubdivision> GeoPaisSubdivision { get; set; }
    }
}
