﻿import React, { Component } from 'react';
import Modal from 'react-modal';
import Button from '@material-ui/core/Button';


export default class PopUpInformativo extends React.Component {

    render() {
        const {  Mesaje } = this.props;
        //const [show, setShow] = useState(false);

        //const handleClose = () => setShow(false);
        //const handleShow = () => setShow(true);
        return (
            

        <Modal animation={false}>
           
            <Modal.Body>{Mesaje}</Modal.Body>
           
        </Modal>
        );


    }
}

//function Example() {
//    const [show, setShow] = useState(false);

//    const handleClose = () => setShow(false);
//    const handleShow = () => setShow(true);

//    return (
//        <>
//            <Button variant="primary" onClick={handleShow}>
//                Launch demo modal
//      </Button>

//            <Modal show={show} onHide={handleClose} animation={false}>
//                <Modal.Header closeButton>
//                    <Modal.Title>Modal heading</Modal.Title>
//                </Modal.Header>
//                <Modal.Body>Woohoo, you're reading this text in a modal!</Modal.Body>
//                <Modal.Footer>
//                    <Button variant="secondary" onClick={handleClose}>
//                        Close
//          </Button>
//                    <Button variant="primary" onClick={handleClose}>
//                        Save Changes
//          </Button>
//                </Modal.Footer>
//            </Modal>
//        </>
//    );
//}

//render(<Example />);