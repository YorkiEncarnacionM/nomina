﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ModeloApi.Models
{
    [Table("AGN_DIRECCION")]
    public partial class AgnDireccion
    {
        [Key]
        public int IdDireccionAgn { get; set; }
        public int IdAgn { get; set; }
        [Required]
        [StringLength(128)]
        public string NombreDireccionAgn { get; set; }
        public bool PrincipalDireccionAgn { get; set; }
        [StringLength(512)]
        public string DireccionAgn { get; set; }
        [StringLength(10)]
        public string CodigoPostalAgn { get; set; }
        [Required]
        [StringLength(2)]
        public string CodigoPaisAlfa2 { get; set; }
        [Required]
        [StringLength(5)]
        public string CodigoSubdivision { get; set; }
        [Column(TypeName = "decimal(10, 6)")]
        public decimal? LatitudDireccionAgn { get; set; }
        [Column(TypeName = "decimal(10, 6)")]
        public decimal? LongitudDireccionAgn { get; set; }
        public bool BorradoDireccionAgn { get; set; }

        [ForeignKey(nameof(CodigoPaisAlfa2))]
        [InverseProperty(nameof(GeoPais.AgnDireccion))]
        public virtual GeoPais CodigoPaisAlfa2Navigation { get; set; }
        [ForeignKey(nameof(CodigoSubdivision))]
        [InverseProperty(nameof(GeoPaisSubdivision.AgnDireccion))]
        public virtual GeoPaisSubdivision CodigoSubdivisionNavigation { get; set; }
        [ForeignKey(nameof(IdAgn))]
        [InverseProperty(nameof(AgnAgente.AgnDireccion))]
        public virtual AgnAgente IdAgnNavigation { get; set; }
    }
}
