﻿const proxy = require("http-proxy-middleware");

module.exports = function (app) {
    app.use(
        proxy("/IpLocation", {
            target: "https://geoip-db.com/jsonp",
            secure:false,
            changeOrigin:true
        })
    );
};

module.exports = function (app) {
    app.use(
        proxy("/api/*", {
            target: "https://localhost:44336",
            secure: false,
            changeOrigin: true
        })
    );
};