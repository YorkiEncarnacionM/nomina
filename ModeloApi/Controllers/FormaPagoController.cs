﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;
//using Microsoft.AspNetCore.Authorization;
//using Microsoft.AspNetCore.Http;
//using Microsoft.AspNetCore.Mvc;
//using Microsoft.EntityFrameworkCore;
//using ModeloApi.Context;
//using ModeloApi.Helpers;

//namespace ModeloApi.Controllers
//{
//    [Route("api/[controller]")]
//    [ApiController]
//    public class FormaPagoController : ControllerBase
//    {
//        private readonly ContextBD _context;

//        public FormaPagoController(ContextBD context)
//        {
//            _context = context;
//        }

//        // GET: api/FormaPago
//        [HttpGet]

//        public async Task<ActionResult<IEnumerable<FormaPago>>> GetFormaPago()
//        {
//            return await _context.FormaPago.ToListAsync();
//        }

//        // GET: api/FormaPago/5
//        [HttpGet("{id}")]
//        //[Authorize(Policy = Policies.User)]
//        [Authorize]
//        public async Task<ActionResult<FormaPago>> GetFormaPago(int id)
//        {
//            var formaPago = await _context.FormaPago.FindAsync(id);

//            if (formaPago == null)
//            {
//                return NotFound();
//            }

//            return formaPago;
//        }

//        // PUT: api/FormaPago/5
//        // To protect from overposting attacks, enable the specific properties you want to bind to, for
//        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
//        [HttpPut("{id}")]
//        [Authorize]
//        public async Task<IActionResult> PutFormaPago(int id, FormaPago formaPago)
//        {
//            if (id != formaPago.IdFormaPago)
//            {
//                return BadRequest();
//            }

//            _context.Entry(formaPago).State = EntityState.Modified;

//            try
//            {
//                await _context.SaveChangesAsync();
//            }
//            catch (DbUpdateConcurrencyException)
//            {
//                if (!FormaPagoExists(id))
//                {
//                    return NotFound();
//                }
//                else
//                {
//                    throw;
//                }
//            }

//            return NoContent();
//        }

//        // POST: api/FormaPago
//        // To protect from overposting attacks, enable the specific properties you want to bind to, for
//        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
//        [HttpPost]
//        [Authorize]
//        public async Task<ActionResult<FormaPago>> PostFormaPago(FormaPago formaPago)
//        {
//            _context.FormaPago.Add(formaPago);
//            await _context.SaveChangesAsync();

//            return CreatedAtAction("GetFormaPago", new { id = formaPago.IdFormaPago }, formaPago);
//        }

//        // DELETE: api/FormaPago/5
//        [HttpDelete("{id}")]
//        [Authorize]
//        public async Task<ActionResult<FormaPago>> DeleteFormaPago(int id)
//        {
//            var formaPago = await _context.FormaPago.FindAsync(id);
//            if (formaPago == null)
//            {
//                return NotFound();
//            }

//            _context.FormaPago.Remove(formaPago);
//            await _context.SaveChangesAsync();

//            return formaPago;
//        }

//        [Authorize]
//        private bool FormaPagoExists(int id)
//        {
//            return _context.FormaPago.Any(e => e.IdFormaPago == id);
//        }
//    }
//}
