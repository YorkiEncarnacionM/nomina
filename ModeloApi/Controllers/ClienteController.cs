﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;
//using Microsoft.AspNetCore.Authorization;
//using Microsoft.AspNetCore.Http;
//using Microsoft.AspNetCore.Mvc;
//using Microsoft.EntityFrameworkCore;
//using ModeloApi.Context;

//namespace ModeloApi.Controllers
//{
//    [Route("api/[controller]")]
//    [ApiController]
//    public class ClienteController : ControllerBase
//    {
//        private readonly ContextBD _context;

//        public ClienteController(ContextBD context)
//        {
//            _context = context;
//        }

//        // GET: api/Cliente
//        [HttpGet]
//        [Authorize]
//        public async Task<ActionResult<IEnumerable<Cliente>>> GetCliente()
//        {
//            return await _context.Cliente.ToListAsync();
//        }

//        // GET: api/Cliente/5
//        [HttpGet("{id}")]
//        [Authorize]
//        public async Task<ActionResult<Cliente>> GetCliente(int id)
//        {
//            var cliente = await _context.Cliente.FindAsync(id);

//            if (cliente == null)
//            {
//                return NotFound();
//            }

//            return cliente;
//        }

//        // PUT: api/Cliente/5
//        // To protect from overposting attacks, enable the specific properties you want to bind to, for
//        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
//        [HttpPut("{id}")]
//        [Authorize]
//        public async Task<IActionResult> PutCliente(int id, Cliente cliente)
//        {
//            if (id != cliente.IdCliente)
//            {
//                return BadRequest();
//            }

//            _context.Entry(cliente).State = EntityState.Modified;

//            try
//            {
//                await _context.SaveChangesAsync();
//            }
//            catch (DbUpdateConcurrencyException)
//            {
//                if (!ClienteExists(id))
//                {
//                    return NotFound();
//                }
//                else
//                {
//                    throw;
//                }
//            }

//            return NoContent();
//        }

//        // POST: api/Cliente
//        // To protect from overposting attacks, enable the specific properties you want to bind to, for
//        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
//        [HttpPost]
//        [Authorize]
//        public async Task<ActionResult<Cliente>> PostCliente(Cliente cliente)
//        {
//            _context.Cliente.Add(cliente);
//            await _context.SaveChangesAsync();

//            return CreatedAtAction("GetCliente", new { id = cliente.IdCliente }, cliente);
//        }

//        // DELETE: api/Cliente/5
//        [HttpDelete("{id}")]
//        [Authorize]
//        public async Task<ActionResult<Cliente>> DeleteCliente(int id)
//        {
//            var cliente = await _context.Cliente.FindAsync(id);
//            if (cliente == null)
//            {
//                return NotFound();
//            }

//            _context.Cliente.Remove(cliente);
//            await _context.SaveChangesAsync();

//            return cliente;
//        }

//        [Authorize]
//        private bool ClienteExists(int id)
//        {
//            return _context.Cliente.Any(e => e.IdCliente == id);
//        }
//    }
//}
