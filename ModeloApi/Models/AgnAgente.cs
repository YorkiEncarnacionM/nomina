﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ModeloApi.Models
{
    [Table("AGN_AGENTE")]
    public partial class AgnAgente
    {
        public AgnAgente()
        {
            AgnDireccion = new HashSet<AgnDireccion>();
            AgnMedioContacto = new HashSet<AgnMedioContacto>();
        }

        [Key]
        public int IdAgn { get; set; }
        public int IdTipoDocumento { get; set; }
        [Required]
        [StringLength(30)]
        public string DocumentoAgn { get; set; }
        [Required]
        [StringLength(128)]
        public string NombresPersonaFisicaAgn { get; set; }
        [Required]
        [StringLength(128)]
        public string ApellidosPersonafisicaAgn { get; set; }
        [StringLength(128)]
        public string NombreFullAgn { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? FechaNacimientoAgn { get; set; }
        public bool BorradoAgn { get; set; }

        [InverseProperty("IdAgnNavigation")]
        public virtual AgnEmpleados AgnEmpleados { get; set; }
        [InverseProperty("IdAgnNavigation")]
        public virtual ICollection<AgnDireccion> AgnDireccion { get; set; }
        [InverseProperty("IdAgnNavigation")]
        public virtual ICollection<AgnMedioContacto> AgnMedioContacto { get; set; }
    }
}
