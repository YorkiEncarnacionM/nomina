import React, { Component } from 'react';
import TextField from '@material-ui/core/TextField';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import { DataGrid } from '@material-ui/data-grid';
import "./posicion.css";
import { makeStyles } from '@material-ui/core/styles';
import axios from 'axios';
import Cookies from 'universal-cookie';
import { XGrid } from '@material-ui/x-grid';

const cookies = new Cookies();
const useStyles = makeStyles((theme) => ({
    card: {
        marginBottom:30,
    }
}));

const columns = [
    { field: 'id', headerName: 'ID', width: 70 },
    { field: 'nombrePosicion', headerName: 'First name', width: 130 },
    { field: 'borradoPosicion', headerName: 'Last name', width: 130 }
];

const rows = [
    { id: 1, nombrePosicion: 'Snow', borradoPosicion: 'Jon' },
    { id: 2, nombrePosicion: 'Lannister', borradoPosicion: 'Cersei'},
    { id: 3, nombrePosicion: 'Lannister', borradoPosicion: 'Jaime'},
    { id: 4, nombrePosicion: 'Stark', borradoPosicion: 'Arya'},
];



class PosicionCRUD extends React.Component {
  static displayName = Posicion.name;
    

  render() {
      return (


    <Grid container spacing={2}>
        <Grid item xs={ 9}>
            <TextField  fullWidth label="Nombre Posicion" />

        </Grid>
        <Grid item xs >
            <Button    
            variant="contained"
            color="primary">Guardar</Button>
        </Grid>
    </Grid>


    );
  }
}

class PosicionGRID extends React.Component {
    static displayName = Posicion.name;
    state = {
    columnDefs: [
            { headerName: "id", field: "make" },
            { headerName: "nombrePosicion", field: "model" },
            { headerName: "borradoPosicion", field: "price" }

    ],
    rowData: []
    }

    componentDidMount() {
        const headers = {
            "Accept": "application/json",
            "Content-Type": "application/json",
            "Authorization": "Bearer " + cookies.get('token')
        };

        axios.get(`/api/Posicion`, { headers })
            .then(res => {
                const persons = res.data;
                this.state.rowData = res.data;
                console.log(this.state.rowData );
                console.log(persons);
            })
    }



    render() {

        const { Data } = this.state.rowData;
        
        return (


            <div style={{ height: 400, width: '100%' }}>
                

            </div>


        );
    }
}

export default function Posicion () {
    const classes = useStyles();
    return (
        <form>

            <Card className={classes.card}>
                <CardContent>
                    <h1>Posicion</h1>
                    <PosicionCRUD/>
                </CardContent>
            </Card>

           <PosicionGRID/>

        </form>
        
        
        );
}
