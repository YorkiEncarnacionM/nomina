import React, { Component } from 'react';
import { Container } from 'reactstrap';
import { NavMenu } from './Layout/NavMenu/NavMenu';
import  Footer  from './Layout/Footer/footer';

export class Layout extends Component {
  static displayName = Layout.name;

  render () {
    return (
      <div>
        <NavMenu />
        <Container>
          {this.props.children}
        </Container>
        <Footer/>
      </div>
    );
  }
}
