﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ModeloApi.Models
{
    [Table("GEO_MONEDA_PAIS")]
    public partial class GeoMonedaPais
    {
        [Key]
        public int IdMonedaPais { get; set; }
        [Required]
        [StringLength(2)]
        public string CodigoPaisAlfa2 { get; set; }
        public int IdMoneda { get; set; }

        [ForeignKey(nameof(CodigoPaisAlfa2))]
        [InverseProperty(nameof(GeoPais.GeoMonedaPais))]
        public virtual GeoPais CodigoPaisAlfa2Navigation { get; set; }
        [ForeignKey(nameof(IdMoneda))]
        [InverseProperty(nameof(GeoIsoMonedas.GeoMonedaPais))]
        public virtual GeoIsoMonedas IdMonedaNavigation { get; set; }
    }
}
