import React, { Component } from 'react';
import { BrowserRouter, Route,Switch } from 'react-router-dom';
import Login from '../src/components/Login/Login';
import App from '../src/App';
import { FetchData } from '../src/components/FectchData/FetchData';
import { Counter } from '../src/components/Counter/Counter';
import { Home } from '../src/components/Home/Home';
import { Layout } from '../src/components/Layout';
import  Posicion  from '../src/components/Test/Posicion/Posicion';

function Routes(){
    return (
        
        <BrowserRouter>
            <Switch>
                <Route exact path="/" component={Login} />
                <Layout>
                    <Route exact path="/app" component={App} />
                    <Route path='/home' component={Home} />
                    <Route path='/counter' component={Counter} />
                    <Route path='/posicion' component={Posicion} />
                    <Route path='/fetch-data' component={FetchData} />
                </Layout>
            </Switch>
        </BrowserRouter>   
    );
}

export default Routes
