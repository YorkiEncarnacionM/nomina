﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ModeloApi.Models
{
    [Table("GEO_ISO_MONEDAS")]
    public partial class GeoIsoMonedas
    {
        public GeoIsoMonedas()
        {
            GeoConversionMonedasMonedaDestinoNavigation = new HashSet<GeoConversionMonedas>();
            GeoConversionMonedasMonedaOrigenNavigation = new HashSet<GeoConversionMonedas>();
            GeoMonedaPais = new HashSet<GeoMonedaPais>();
        }

        [Key]
        public int IdMoneda { get; set; }
        [Required]
        [StringLength(3)]
        public string CodigoMoneda { get; set; }
        [Required]
        [StringLength(128)]
        public string NombreMoneda { get; set; }
        public int? NumeroMoneda { get; set; }
        public int? DecimalesMoneda { get; set; }
        [StringLength(10)]
        public string SimboloMoneda { get; set; }
        public bool LocalMoneda { get; set; }

        [InverseProperty(nameof(GeoConversionMonedas.MonedaDestinoNavigation))]
        public virtual ICollection<GeoConversionMonedas> GeoConversionMonedasMonedaDestinoNavigation { get; set; }
        [InverseProperty(nameof(GeoConversionMonedas.MonedaOrigenNavigation))]
        public virtual ICollection<GeoConversionMonedas> GeoConversionMonedasMonedaOrigenNavigation { get; set; }
        [InverseProperty("IdMonedaNavigation")]
        public virtual ICollection<GeoMonedaPais> GeoMonedaPais { get; set; }
    }
}
