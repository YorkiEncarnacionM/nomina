﻿
import React, { Component } from 'react';

import './footer.css';
import Copyright from '../Copyright/Copyright';
import Grid from '@material-ui/core/Grid';


class Footer extends React.Component {

    render() {

        return (

            <footer>

                <Grid
                    container
                    direction="row"
                    justify="space-between"
                    alignItems="center"
                >
                    <div>

                    </div>
                    <div>
                        <Copyright />
                    </div>
                    <div>
                    </div>
                   
                  
  
                </Grid>
                
                
               
            </footer>

        )

    }

}

export default Footer;