﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;
//using Microsoft.AspNetCore.Http;
//using Microsoft.AspNetCore.Mvc;
//using Microsoft.EntityFrameworkCore;
//using ModeloApi.Context;

//namespace ModeloApi.Controllers
//{
//    [Route("api/[controller]")]
//    [ApiController]
//    public class AlmacenController : ControllerBase
//    {
//        private readonly ContextBD _context;

//        public AlmacenController(ContextBD context)
//        {
//            _context = context;
//        }

//        // GET: api/Almacen
//        [HttpGet]
//        public async Task<ActionResult<IEnumerable<Almacen>>> GetAlmacen()
//        {
//            return await _context.Almacen.ToListAsync();
//        }

//        // GET: api/Almacen/5
//        [HttpGet("{id}")]
//        public async Task<ActionResult<Almacen>> GetAlmacen(int id)
//        {
//            var almacen = await _context.Almacen.FindAsync(id);

//            if (almacen == null)
//            {
//                return NotFound();
//            }

//            return almacen;
//        }

//        // PUT: api/Almacen/5
//        // To protect from overposting attacks, enable the specific properties you want to bind to, for
//        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
//        [HttpPut("{id}")]
//        public async Task<IActionResult> PutAlmacen(int id, Almacen almacen)
//        {
//            if (id != almacen.IdAlmacen)
//            {
//                return BadRequest();
//            }

//            _context.Entry(almacen).State = EntityState.Modified;

//            try
//            {
//                await _context.SaveChangesAsync();
//            }
//            catch (DbUpdateConcurrencyException)
//            {
//                if (!AlmacenExists(id))
//                {
//                    return NotFound();
//                }
//                else
//                {
//                    throw;
//                }
//            }

//            return NoContent();
//        }

//        // POST: api/Almacen
//        // To protect from overposting attacks, enable the specific properties you want to bind to, for
//        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
//        [HttpPost]
//        public async Task<ActionResult<Almacen>> PostAlmacen(Almacen almacen)
//        {
//            _context.Almacen.Add(almacen);
//            await _context.SaveChangesAsync();

//            return CreatedAtAction("GetAlmacen", new { id = almacen.IdAlmacen }, almacen);
//        }

//        // DELETE: api/Almacen/5
//        [HttpDelete("{id}")]
//        public async Task<ActionResult<Almacen>> DeleteAlmacen(int id)
//        {
//            var almacen = await _context.Almacen.FindAsync(id);
//            if (almacen == null)
//            {
//                return NotFound();
//            }

//            _context.Almacen.Remove(almacen);
//            await _context.SaveChangesAsync();

//            return almacen;
//        }

//        private bool AlmacenExists(int id)
//        {
//            return _context.Almacen.Any(e => e.IdAlmacen == id);
//        }
//    }
//}
