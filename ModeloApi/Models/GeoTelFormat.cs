﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ModeloApi.Models
{
    [Table("GEO_TEL_FORMAT")]
    public partial class GeoTelFormat
    {
        [Key]
        public int IdFormatTel { get; set; }
        [Required]
        [StringLength(2)]
        public string CodigoPaisAlfa2 { get; set; }
        [StringLength(40)]
        public string MascaraMovil { get; set; }
        [StringLength(40)]
        public string MascaraFijo { get; set; }
        [StringLength(40)]
        public string FormatoMovil { get; set; }
        [StringLength(40)]
        public string FormatoFijo { get; set; }

        [ForeignKey(nameof(CodigoPaisAlfa2))]
        [InverseProperty(nameof(GeoPais.GeoTelFormat))]
        public virtual GeoPais CodigoPaisAlfa2Navigation { get; set; }
    }
}
