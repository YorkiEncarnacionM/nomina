﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ModeloApi.Models
{
    [Table("AGN_MEDIO_CONTACTO")]
    public partial class AgnMedioContacto
    {
        [Key]
        public int IdMedioContacto { get; set; }
        public int IdAgn { get; set; }
        public int IdTipoMedioContacto { get; set; }
        [StringLength(256)]
        public string MedioContacto { get; set; }
        public bool BorradoMedioContacto { get; set; }

        [ForeignKey(nameof(IdAgn))]
        [InverseProperty(nameof(AgnAgente.AgnMedioContacto))]
        public virtual AgnAgente IdAgnNavigation { get; set; }
        [ForeignKey(nameof(IdTipoMedioContacto))]
        [InverseProperty(nameof(AgnTipoMedioContato.AgnMedioContacto))]
        public virtual AgnTipoMedioContato IdTipoMedioContactoNavigation { get; set; }
    }
}
