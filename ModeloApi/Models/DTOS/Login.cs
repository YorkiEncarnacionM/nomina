﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ModeloApi.Models.DTOS
{
    public class Login
    {

        [Required]
        [StringLength(50)]
        public string Usuario { get; set; }
        [Required]
        [StringLength(128)]
        public string ClaveUsuario { get; set; }
        
    }
}
