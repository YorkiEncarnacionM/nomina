﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ModeloApi.Models
{
    [Table("AGN_TIPO_MEDIO_CONTATO")]
    public partial class AgnTipoMedioContato
    {
        public AgnTipoMedioContato()
        {
            AgnMedioContacto = new HashSet<AgnMedioContacto>();
        }

        [Key]
        public int IdTipoMedioContacto { get; set; }
        public int IdCategoriaMedioContacto { get; set; }
        [Required]
        [StringLength(128)]
        public string NombreTipoMedioContacto { get; set; }
        public byte[] IconoTipoMedioContacto { get; set; }
        [StringLength(30)]
        public string FavIconTipoMedioContacto { get; set; }
        public bool BorradoTipoMedioContacto { get; set; }
        [StringLength(30)]
        public string MascaraTipoMedioContacto { get; set; }
        [StringLength(30)]
        public string FormatoTipoMedioContacto { get; set; }

        [InverseProperty("IdTipoMedioContactoNavigation")]
        public virtual ICollection<AgnMedioContacto> AgnMedioContacto { get; set; }
    }
}
