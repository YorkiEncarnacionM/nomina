﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ModeloApi.Models
{
    [Table("SYS_USUARIO")]
    public partial class SysUsuario
    {
        [Key]
        public int IdUsuario { get; set; }
        [Required]
        [StringLength(128)]
        public string NombreUsuario { get; set; }
        [Required]
        [StringLength(128)]
        public string ApellidoUsuario { get; set; }
        [Required]
        [StringLength(50)]
        public string Usuario { get; set; }
        [Required]
        [StringLength(128)]
        public string ClaveUsuario { get; set; }
        [Required]
        [StringLength(256)]
        public string EmailUsuario { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime FechaCreacionUsuario { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? FechaBajaUsuario { get; set; }
        [StringLength(2048)]
        public string TokenUsuario { get; set; }
        public bool BorradoUsuario { get; set; }
        public byte[] SecurityKey { get; set; }
    }
}
